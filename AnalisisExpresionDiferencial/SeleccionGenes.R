library(VennDiagram)
deseq2 <- read.csv("ResultadoDESeq2.csv",sep = ";")
limma <- read.csv("ResultadoLimma1.csv",sep = ";")
NanostringDiff <- read.csv("ResultadoNanoStringDiff.csv",sep = ";")

res <- venn.diagram(list(deseq2$x,limma$x,NanostringDiff$x) ,
                          NULL, fill=c("blue", "green","red"), 
                          alpha=c(0.5,0.5,0.5), cex = 2, 
                          cat.fontface=4,
                          category.names=c("DESeq2", 
                                           "Limma",
                                           "NanoStringDiff"), 
                          main="Conjunto de genes")
grid.draw(res)

ListaGenes <- c(as.character(deseq2$x),
                as.character(limma$x),
                as.character(NanostringDiff$x))
#Eliminamos duplicados
ListaGenes <- unique(ListaGenes)
write.csv(ListaGenes,"ListaGenesDefinitiva.csv")
